using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meta : MonoBehaviour
{
    public GameObject _meta;
    // Start is called before the first frame update
    void Start()
    {
        _meta.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        _meta.SetActive(true);
        Time.timeScale = 0.0f;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Barradevida : MonoBehaviour
{
    private Slider slider;

    private void Start()
    {
        slider = GetComponent<Slider>(); 
    }
    public void Cambiarvidamaxima(float vidaMaxima)
    {
        slider.maxValue = vidaMaxima;
    }

    public void CambiarvidaActual(float cantidadVida)
    {
        slider.value = cantidadVida;
    }

    public void InicializarBarradeVida(float cantidadVida)
    {
        Cambiarvidamaxima(cantidadVida);
        CambiarvidaActual(cantidadVida);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aparecerydesaparecer : MonoBehaviour
{
    public float tiempoDesaparicion = 2f; // Tiempo después del cual la plataforma desaparecerá
    public float tiempoReaparicion = 5f; // Tiempo después del cual la plataforma reaparecerá
    private float timer = 0f; // Temporizador para realizar un seguimiento del tiempo transcurrido
    private Renderer plataformaRenderer; // Componente Renderer de la plataforma
    private BoxCollider2D colisionP;

    void Start()
    {
        // Obtener el componente Renderer de la plataforma
        plataformaRenderer = GetComponent<Renderer>();

        // Inicializar la plataforma como visible
        plataformaRenderer.enabled = true;

        colisionP = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        // Incrementar el temporizador
        timer += Time.deltaTime;

        // Si el temporizador alcanza el tiempo de desaparición, ocultar la plataforma
        if (timer >= tiempoDesaparicion && plataformaRenderer.enabled)
        {
            plataformaRenderer.enabled = false;
            timer = 0f; // Reiniciar el temporizador para contar el tiempo de reaparición
            colisionP.enabled = false;
        }

        // Si el temporizador alcanza el tiempo de reaparición, mostrar la plataforma
        if (timer >= tiempoReaparicion && !plataformaRenderer.enabled)
        {
            plataformaRenderer.enabled = true;
            timer = 0f; // Reiniciar el temporizador para contar el tiempo de desaparición
            colisionP.enabled = true;
        }
    }
}
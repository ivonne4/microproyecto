using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguirJugador : MonoBehaviour
{
    public Transform jugador; // Referencia al objeto del jugador

    public float suavizado = 0.1f; // Velocidad a la que la c�mara sigue al jugador

    private Vector3 offset; // Distancia entre la c�mara y el jugador

    void Start()
    {
        // Calcula la distancia inicial entre la c�mara y el jugador
        offset = transform.position - jugador.position;
    }

    void FixedUpdate()
    {
        if (jugador != null) // Aseg�rate de que el jugador existe
        {
            // Calcula la posici�n deseada de la c�mara
            Vector3 posicionDeseada = jugador.position + offset;

            // Interpola suavemente entre la posici�n actual y la posici�n deseada
            Vector3 nuevaPosicion = Vector3.Lerp(transform.position, posicionDeseada, suavizado);

            // Establece la nueva posici�n de la c�mara
            transform.position = nuevaPosicion;
        }
    }
}

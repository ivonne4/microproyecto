using UnityEngine;

public class bj : MonoBehaviour
{
    public float speed = 5f; // Velocidad del movimiento del jugador
    public float jumpForce = 10f; // Fuerza aplicada al saltar
    private bool isGrounded; // Bandera para rastrear si el jugador est� en el suelo

    void Update()
    {
        // Movimiento horizontal
        float horizontalInput = Input.GetAxis("Horizontal");

        // Calcula la direcci�n del movimiento horizontal
        Vector3 movement = new Vector3(horizontalInput, 0f, 0f) * speed * Time.deltaTime;

        // Aplica el movimiento horizontal
        transform.Translate(movement);

        // Salto
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            Jump();
        }
    }

    void Jump()
    {
        // Aplica la fuerza de salto si el jugador est� en el suelo
        if (isGrounded)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            isGrounded = false; // El jugador ya no est� en el suelo despu�s de saltar
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Verifica si el jugador colisiona con el suelo
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true; // El jugador est� en el suelo
        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{

    [SerializeField] private float vida;

    [SerializeField] private float maximoVida;

    [SerializeField] private Barradevida barradevida;

    private void Start()
    {
        vida = maximoVida;
        barradevida.InicializarBarradeVida(vida);
    }

    public void TomarDa�o(float da�o)
    {
        vida -= da�o;
        barradevida.CambiarvidaActual(vida);
        if (vida <= 0)
        {
            Destroy(gameObject);
        }
    }


}
